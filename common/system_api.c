
#include "appinc.h"

uint32_t libsys_get_uptime(void)
{
	struct timespec tv;

	clock_gettime(CLOCK_MONOTONIC, &tv);

	return tv.tv_sec;
}


uint64_t libsys_get_uptime_ms(void)
{
	uint64_t uptime;
	struct timespec tv;

	clock_gettime(CLOCK_MONOTONIC, &tv);

	uptime = tv.tv_sec;
	uptime *= 1000;
	uptime += tv.tv_nsec/1000000;

	return uptime;
}

