
APP_TOPDIR = $(shell pwd)
APP_OUTDIR ?= $(APP_TOPDIR)/out

APP_OBJ := $(APP_OUTDIR)/obj
APP_DESTROOT := $(APP_OUTDIR)/install
RELEASE_DIR := $(CUR_DIR)/release
APP_DESTBIN := $(APP_DESTROOT)/usr/sbin

export APP_TOPDIR APP_OBJ APP_APPDIR APP_PREBUILD APP_DESTROOT APP_DESTBIN


include config.mk

APP_CFLAGS := -I$(APP_TOPDIR)/include -O2
APP_LDFLAGS := -lpthread -lm
export APP_CFLAGS APP_LDFLAGS

subdir-y := demo


include $(APP_TOPDIR)/rule.mk
$(eval $(call build_subdir,$(subdir-y)))

