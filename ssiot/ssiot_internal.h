#ifndef __SSIOT_INTERNAL_H__
#define __SSIOT_INTERNAL_H__

#include "mqtt.h"

#define SSIOT_DATA_CACHE_SIZE   (10*1024*1024)
#define SSIOT_SDK_VERSION       "V1.2.1"

typedef struct {
	struct list_head list;
	char topic[MQTT_TOPIC_LEN];
	uint32_t size;
	uint8_t data[0];
}ssiot_pkg_t;


typedef struct ssiot_s {
	char				devsn[32];
	char				model[32];
	char				*key;
	notify_event_t		notify;
	struct list_head	pkg_list;
	pthread_mutex_t		pkg_mutex;
	int					cache_max;
	int					cache_size;

	void (*on_message)(ssiot_message_t *, void *);
	void *msg_arg;

	pthread_t      mqtt_thread;
	pthread_t      data_thread;
	mqtt_context_t mqtt;
	mqtt_param_t   param;
	uint64_t       next_time;    //下一次重新尝试时间
	uint32_t       connect_time; //socket连接成功时间
	uint32_t       fails;        //连接失败次数
	uint32_t       retry;        //当前步骤尝试次数
	int            sid_s;
	char           *cur_src;
	char           *to;
	cJSON          *resp_data;
	int            resp_code;
	char           *resp_msg;

	bool           on_goto;     //当前是否工作在goto服务器上
	char           *goto_server;
	uint32_t       goto_time;   //下次跳转时间（异常使用）
	uint32_t       goto_fails;  //跳转失败次数

	bool           logined;
	bool           connected;

	bool           running;
	uint32_t       uptimes; //连上平台次数
	uint32_t       online; //连上平台的时间
	uint32_t       offline; //掉线的时间
}ssiot_t;


#endif
