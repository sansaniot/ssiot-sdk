# 介绍
三三物联设备端SDK，用于简化终端设备接入平台的开发

该SDK只适用linux环境，该分支适用于单进程实现多业务

# 代码文件

```
common     封装的一些系统api
include    一些需要的头文件
ssiot      SDK核心代码
demo       示例代码
config.mk  编译环境配置
rule.mk    编译规则，简化其它makefile文件
```

# 使用步骤

1. 调用ssiot_create，传入连接参数，同时注册回调函数

   - SDK内部会维持连接并完成登录和跳转等操作，并且掉线后会自动重连
   - 重连等待间隔时间会递增，最大30分钟（有需要可以自己修改重连策略）
2. 在上一步注册的回调函数中实现自己的业务
   - 如果需要按三三协议回复ACK时，设置msg的resp_data，resp_code，resp_msg等
   - SDK内部会完成组包和发送ACK
3. 使用ssiot_publish或ssiot_send_data发送业务数据
   - 当设备掉线时，发送的数据会缓存
   - 当前缓存大小10MB，由SSIOT_DATA_CACHE_SIZE配置，有需要可以修改
4. 修改LOG接口，控制日志输出（如果有需要）

# 使用限制

- 主题最大长度128字节，由MQTT_TOPIC_LEN配置，有需要可以修改
- 接收最大包长1MB，由RECIVER_MAX_SIZE配置，有需要可以修改
- 使用TLS加密传输时不校验证书，有需要可以自己增加校验代码