#ifndef __NOTIFY_API_H__
#define __NOTIFY_API_H__


typedef struct {
    pthread_mutex_t     mutex;
    pthread_cond_t      cond;
    uint32_t            flags;
} notify_event_t;


/* 初始化一个event */
int libnotify_init(notify_event_t *event);

/* 等待事件,标志有变化则退出
timeout: 等待的超时时间，单位：ms, 如果<=0, 表示一直等待
return: 0：等待成功
*/
int libnotify_wait(notify_event_t *event, int timeout);


/* 产生空事件 */
int libnotify_wakeup(notify_event_t *event);


#endif
