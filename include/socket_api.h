#ifndef __SOCKET_API_H__
#define __SOCKET_API_H__


/* 创建TCP监听，port==0表示使用unix域，此时addr是文件路径，num是监听大小*/
int libsocket_listen_tcp(const char *addr, uint16_t port, int nonblock, int num);

/* 创建UDP监听，port==0表示使用unix域，此时addr是文件路径*/
int libsocket_listen_udp(const char *addr, uint16_t port, int nonblock);

/* 创建TCP连接，port==NULL表示使用unix域，此时addr是文件路径, timeout:单位ms*/
int libsocket_connect_tcp(const char *addr, const char *port, int timeout);

/* 创建UDP连接，port==NULL表示使用unix域，此时addr是文件路径, timeout:单位ms*/
int libsocket_connect_udp(const char *addr, const char *port, int timeout);


#endif
