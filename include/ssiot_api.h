#ifndef __SSIOT_API_H__
#define __SSIOT_API_H__

typedef struct ssiot_s *ssiot_handel;

typedef struct {
	char *product;      //产品编码，由平台分配
	char *devsn;        //设备唯一SN
	char *key;          //登录key
	char *server;       //服务器地址,tls加密连接时使用mqtts://或tls://开头
	char *username;
	char *password;
}ssiot_param_t;


enum {
	SSIOT_MSG_NONE,
	SSIOT_MSG_CONNECTED,
	SSIOT_MSG_DISCONNECTED,
	SSIOT_MSG_PAYLOAD,
	SSIOT_MSG_JSON,
	SSIOT_MSG_MAX,
};

enum {
	SSCODE_OK = 200,
	SSCODE_ERR_REQUEST = 400,
	SSCODE_ERR_FOUND = 404,
	SSCODE_ERR_SERVER = 500,
};

typedef struct {
	int        type;       //消息类型
	char       *topic;     //消息子主题,
	void       *payload;   //消息原始数据
	int        payloadlen; //消息原始数据长度
	//请求
	char       *src;       //主题中的源地址
	int        sid;        //会话ID
	char       *to;        //子设备
	cJSON      *data;      //消息的数据体
	//响应
	cJSON      *resp_data; //响应数据，不为null会发ack到平台
	int        resp_code;  //响应编码，同上
	char       *resp_msg;  //响应消息，可用于错误说明
	bool       free_msg;   //msg由上层malloc，使用完后要free
}ssiot_message_t;

//以下API，handel为NULL时使用第一个创建的句柄

//topic:以‘/’开头时直接使用，否则终以/iot/$/sn/topic进行发布
int ssiot_publish(ssiot_handel handel, const char *topic, void *data, int len);

//封装一个接口，用于发送业务数据, sid小于0时不传输，type:业务主题
int ssiot_send_data(ssiot_handel handel, const char *from, int sid, char *type, cJSON *data);

//on_message尽量不要阻塞,返回NULL表示创建失败
ssiot_handel ssiot_create(ssiot_param_t *param, void (*on_message)(ssiot_message_t *, void *), void *arg);

//销毁一个创建的句柄
int ssiot_destory(ssiot_handel handel);

//重新初始化连接参数
int ssiot_reinit(ssiot_handel handel, ssiot_param_t *param);


#endif
