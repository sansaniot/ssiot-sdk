#ifndef __APP_INCLUDE_H__
#define __APP_INCLUDE_H__


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <syslog.h>
#include <dirent.h>
#include <signal.h>
#include <netdb.h>
#ifndef _LINUX_IF_H
#include <net/if.h>
#endif
#include <net/route.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdarg.h>
#include <sys/un.h>
#include <sys/reboot.h>
#include <elf.h>
#include <pthread.h>
#include <assert.h>
#include "list.h"
#include "cJSON.h"

#include "string_api.h"
#include "notify_api.h"
#include "file_api.h"
#include "socket_api.h"
#include "system_api.h"


//#define LOG_D(format, ...)	do{printf(format "%s", ##__VA_ARGS__, "\n");}while(0)
#define LOG_I(format, ...)	do{printf(format "%s", ##__VA_ARGS__, "\n");}while(0)
#define LOG_W(format, ...)	do{printf(format "%s", ##__VA_ARGS__, "\n");}while(0)
#define LOG_E(format, ...)	do{printf(format "%s", ##__VA_ARGS__, "\n");}while(0)

#define LOG_D(...) do{}while(0)


#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#endif
#define WAITING_FOREVER        (-1U)
#define __weak __attribute__((weak))
#define TOSTR(x...) #x
#define ABS(x) ((x) < 0 ? -(x) : (x))


#endif

