#ifndef __STRING_API_H__
#define __STRING_API_H__

#ifndef HAVE_STRLCPY
size_t strlcpy(char *dest, const char *src, size_t dest_size);
#endif

/*将字符串转成大写*/	   
void libstr_toupper(char *str);

/*将字符串转成小写*/
void libstr_tolowr(char *str);

/*去掉字符串开始和结尾的回车、换行、空格、TAB */
void libstr_trim(char *a_line);

/* 移除行尾的\r、\n */
void libstr_del_crlf(char *line);

/* 从路径中获取文件名 */
char *libstr_path_notdir(char *path);

/* 检查字符串是否是IPV4地址, 返回1表示是IPV4，反之不是 */
int libstr_is_ipv4(const char *ip);

/* 检查字符串是否是IPV6地址, 返回1表示是IPV4，反之不是 */
int libstr_is_ipv6(const char *ip);

/* 检查字符串是否是IP地址, 返回1表示是IP，反之不是 */
int libstr_is_ip(const char *ip);

/* 检查字符串是否是IP地址+掩码, 返回1表示是IP，反之不是 */
int libstr_is_ip_mask(const char *ip_mask);

/* 检查字符串是否是MAC地址, 返回1表示是MAC，反之不是 */
int libstr_is_mac(const char *mac);

/* 十六进制与字符串转换,需要上层确保有足够的空间 */
int libstr_mac_to_str(uint8_t *mac, char *str);

/* 从字符串中获取IP和掩码, 返回0成功，其它失败 */
int libstr_get_ip_mask(const char *buf, char *ip, char *mask);

/* 获取字符串格式掩码长度 */
int libstr_mask_to_len(const char *smask);

/* 检查一个字符串是否全部是数字, 如果是则返回1，否则返回0 */
int libstr_is_digit(const char *str);

/* 检查一个字符串是否为除空格以外的可打印字符, 如果是则返回1，否则返回0 */
int libstr_is_graph(const char *str);

/* 十六进制与字符串转换 */
int libstr_str_to_hex(const char *str, uint8_t *hex);
int libstr_hex_to_str(uint8_t *hex, uint32_t hex_len, char *str);

/* 判读字符串口bool类型，返回0和1表示成功，其它表示失败 */
bool libstr_to_bool(const char *str, bool def);

//获取一个字符串的后缀
const char *libstr_get_postfix(const char *name);

//比较s1与s2开头部分
int libstr_cmp_prefix(const char *s1, const char *s2);


#endif
