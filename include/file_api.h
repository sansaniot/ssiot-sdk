
#ifndef __FILE_API_H__
#define __FILE_API_H__

/******************************* 描述符操作 *******************************/

/* 设置fd描述符的阻塞状态 */
int libfd_set_nonblock(int fd, bool nonblock);

/* 从指定的fd文件描述符读取数据,fd为非阻塞模式
   max_size: 最大读取长度
   timeout：读取超时等待时间，单位ms */
int libfd_read(int fd, void *buf, int max_size, int timeout);

/* 向指定的fd文件描述符写入数据,fd为非阻塞模式*/
int libfd_write(int fd, const void *buf, int size, int timeout);

/* 从指定的fd文件描述符读取数据包,fd为非阻塞模式
   max_size: 最大读取长度
   timeout：读取超时等待时间，单位ms */
int libfd_read_package(int fd, void *buf, int max_size, int timeout);



/******************************* 文件操作 *******************************/

/* 获取文件的大小 */
long libfile_get_size(const char *name);

/* 检测一个文件是不是ASCII文件，返回1表示是，其它不是 */
int libfile_is_ascii(const char *path);

/* 检测一个文件是不是ELF文件，返回1表示是，其它不是 */
int libfile_is_elf(const char *path);

/* 获取ELF文件的体系结构, 0表示获取失败 */
uint16_t libfile_get_elf_machine(const char *path);

/* 检查ELF文件是中是否有指定的section */
int libfile_check_elf_section(const char *path, const char *section);

/* 从文件中读取字符数字 */
int libfile_read_num(const char *file, int base, int *num);

/* 从文件中读取数据 */
int libfile_read_data(const char *file, void *data, int size);

/* 将数据写入文件 */
int libfile_write_data(const char *file, void *data, int size);

/* 从info文件中获取一项数据,file可以是绝对路径也可以是info名(从/tmp目录取文件)
info文件使用冒号表格 */
int libfile_read_info_item(const char *file, const char *name, char *value, int size);


/******************************* 目录操作 *******************************/
/* 检查路径是否是一个目录 */
int libdir_is_dir(const char *path);

/* 获取一个目录的剩余空间 */
uint64_t libdir_get_free_space(const char *path);

/* 获取目录挂载的设备名，返回值小于0表示失败 */
int libdir_get_mount_dev(const char *path, char *dev_name, int max_size);

/* 目录是否是挂载点 */
int libdir_is_mountpoint(const char *path);

/* 获取文件所在目录的文件系统类型 */
long libdir_get_filesystem_type(const char *path);


#endif


