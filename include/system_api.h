#ifndef __SYSTEM_API_H__
#define __SYSTEM_API_H__

uint32_t libsys_get_uptime(void);

uint64_t libsys_get_uptime_ms(void);

#endif
