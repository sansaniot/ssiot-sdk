
PLATFORM ?= x64

ifeq ($(PLATFORM),rk3568_ubuntu)
BUILD_PATH := /opt/toolchain_linaro/gcc-linaro-6.3.1-2017.05-x86_64_aarch64-linux-gnu/bin:$(PATH)
export PATH=$(BUILD_PATH)
CROSS_COMPILE = aarch64-linux-gnu-
endif



MAKE := make
CC := $(CROSS_COMPILE)gcc
CXX:= $(CROSS_COMPILE)g++
AR := $(CROSS_COMPILE)ar
AS := $(CROSS_COMPILE)as
LD := $(CROSS_COMPILE)ld
NM := $(CROSS_COMPILE)nm
STRIP := $(CROSS_COMPILE)strip
CPP := $(CROSS_COMPILE)cpp
OBJCOPY := $(CROSS_COMPILE)objcopy
export MAKE ARCH CROSS_COMPILE_HOST CROSS_COMPILE CC CXX AR AS LD NM RANLIB STRIP CPP OBJCOPY
export PLATFORM KERNELDIR
