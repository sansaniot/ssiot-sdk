#include "appinc.h"
#include "sans_demo.h"


int sans_func_at(void *obj, ssiot_message_t *msg, const char *cmd, int len)
{
	printf("exec at cmd: %s\n", cmd);
	ssiot_publish(NULL, "func/at/ack", "OK\r\n", 4);

	return 0;
}

int sans_func_shell(void *obj, ssiot_message_t *msg, const char *cmd, int len)
{
	printf("exec shell cmd: %s\n", cmd);
	ssiot_publish(NULL, "func/shell/ack", "0\n", 2);

	return 0;
}


static int sans_func_reboot(void *obj, ssiot_message_t *msg, cJSON *data)
{
	printf("server request to reboot\n");

	return 0;
}

static int sans_func_led(void *obj, ssiot_message_t *msg, cJSON *data)
{
	cJSON *json_n = NULL;
	int i, num;

	if (data == NULL) {
		msg->resp_code = SSCODE_ERR_REQUEST;
		msg->resp_msg = "request invalid";
		return -EINVAL;
	}

	num = cJSON_GetArraySize(data);
	for (i = 0; i < num; i++) {
		json_n = cJSON_GetArrayItem(data, i);
		if (json_n->type == cJSON_Number)
			printf("set led %s = %d\n", json_n->string, json_n->valueint);
	}

	msg->resp_code = SSCODE_OK;

	return 0;
}


int sans_func(void *obj, const char *topic, ssiot_message_t *msg)
{
	if (!strcmp(topic, "reboot"))
		return sans_func_reboot(obj, msg, msg->data);
	else if (!strcmp(topic, "led"))
		return sans_func_led(obj, msg, msg->data);

	return -1;
}


