#ifndef __SANS_DEMO_H__
#define __SANS_DEMO_H__

#include "ssiot_api.h"



#define SERVER     "mqtt://ssiot.cc:9010"
#define USERNAME   "ssiot"
#define PASSWORD   "!@#abcd"
#define LOGIN_KEY  "key123"


//通过file/read协议从平台下载文件
#define READ_PKG_SIZE (1024)
struct download_t{
	struct list_head list;
	int sid;
	uint8_t *buf;
	int offset;
	int count;
	notify_event_t event;
};

int sans_props(void *obj, const char *topic, ssiot_message_t *msg);
int sans_sensor(void *obj, const char *topic, ssiot_message_t *msg);
int sans_config(void *obj, const char *topic, ssiot_message_t *msg);
int sans_ota(void *obj, const char *topic, ssiot_message_t *msg);
int sans_func(void *obj, const char *topic, ssiot_message_t *msg);
int sans_file(void *obj, const char *topic, ssiot_message_t *msg);

int sans_file_download(char *file, uint8_t *buf, int size, int timeout);
int sans_stream_recive(void *obj, const char *topic, void *data, int len);
int sans_func_at(void *obj, ssiot_message_t *msg, const char *cmd, int len);
int sans_func_shell(void *obj, ssiot_message_t *msg, const char *cmd, int len);

int sans_ota_inform(void);
int props_demo_send(void);
int sensor_demo_send(void);


#endif
