
#include "appinc.h"
#include "sans_demo.h"

void on_message(ssiot_message_t *msg, void *arg)
{
	void *obj = arg;
	
	if (msg->type == SSIOT_MSG_JSON) {
		printf("this is json data, topic:%s\n", msg->topic);

		//按需实现以下业务
		if (!strncmp(msg->topic, "props/", 6))
			sans_props(obj, msg->topic+6, msg);
		else if (!strncmp(msg->topic, "sensor/", 7))
			sans_sensor(obj, msg->topic+7, msg);
		else if (!strncmp(msg->topic, "config/", 7))
			sans_config(obj, msg->topic+7, msg);
		else if (!strncmp(msg->topic, "ota/", 4))
			sans_ota(obj, msg->topic+4, msg);
		else if (!strncmp(msg->topic, "func/", 5))
			sans_func(obj, msg->topic+5, msg);
		else if (!strncmp(msg->topic, "file/", 5))
			sans_file(obj, msg->topic+5, msg);
		
		//其它自定义json业务
	} else if (msg->type == SSIOT_MSG_PAYLOAD) {
		printf("this is raw payload, size:%d, topic:%s\n", msg->payloadlen, msg->topic);

		//按需实现以下业务
		if (!strncmp(msg->topic, "stream/", 7))
			sans_stream_recive(obj, msg->topic+7,  msg->payload, msg->payloadlen);
		else if (!strcmp(msg->topic, "func/at"))
			sans_func_at(obj, msg, msg->payload, msg->payloadlen);
		else if (!strcmp(msg->topic, "func/shell"))
			sans_func_shell(obj, msg, msg->payload, msg->payloadlen);
	
		//其它自定义非json业务
	}
}


//main用于测试
int main(int argc, char *argv[])
{
	ssiot_param_t param = {"product_demo", "sn_demo123", LOGIN_KEY, SERVER, USERNAME, PASSWORD};
	cJSON *props, *sensor;
	int num = 0;

	ssiot_create(&param, on_message, NULL);

	//启动时上报版本号
	sans_ota_inform();

	//以下代码是发送模拟props和sensor数据
	while (1) {
		props_demo_send();
		sensor_demo_send();
		sleep(15);
	}
}

